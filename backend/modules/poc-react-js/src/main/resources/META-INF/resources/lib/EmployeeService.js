import axios from "axios";
const BASE_LOGIN_URL = "http://localhost:8080/o/authz-rest/";
const BASE_TOKEN_URL = "http://localhost:8080/o/oauth2/token";
const BASE_EMPLOYEE =
  "http://localhost:8080/o/dogoo/employee-rest-builder/v1.0/employees/";

class EmployeeService {
  signIn(username, password, access_token) {
    return axios.post(
      BASE_LOGIN_URL + "signin",
      {
        username: username,
        password: password,
      },
      {
        headers: {
          Accept: "application/json",
          Authorization: " Bearer " + access_token,
        },
      }
    );
  }

  getEmployees(access_token, token_jwt) {
    console.log(token_jwt);
    console.log(access_token);
    const headers = {
      "dogoo-x-user-context-request": access_token,
      Authorization: "Bearer " + token_jwt,
      Accept: "*/*",
      "Content-Type": "application/json",
    };
    console.log(headers);
    return axios.get(BASE_EMPLOYEE, {
      headers: headers,
    });
  }

  getToken() {
    const client_id = "id-17f6e471-6322-5742-663b-36bda0c9b38";
    const client_secret = "secret-283ed4db-f4dc-f124-f428-ff2c8a346738";
    return axios.post(
      BASE_TOKEN_URL +
        "?grant_type=client_credentials&client_id=" +
        client_id +
        "&client_secret=" +
        client_secret,
      {},
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      }
    );
  }
}

export default new EmployeeService();
