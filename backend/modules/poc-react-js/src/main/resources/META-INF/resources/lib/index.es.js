import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import EmployeeService from "./EmployeeService";

export default function (elementId) {
  ReactDOM.render(<Employee />, document.getElementById(elementId));
}

class Employee extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      employees: [],
    };
  }
  componentDidMount() {
    let token_jwt = {};
    EmployeeService.getToken()
      .then((response) => {
        token_jwt = response.data;
        console.log("1: " + token_jwt);
        EmployeeService.signIn("test", "test", token_jwt.access_token)
          .then((response) => {
            const access_token = response.data.accessToken;
            EmployeeService.getEmployees(access_token, token_jwt.access_token)
              .then((response) => {
                this.setState({
                  employees: response.data.items,
                });
              })
              .catch((error) => console.log(error));
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => console.log(error));
    console.log("2: " + token_jwt);
  }

  render() {
    const employeeList = this.state.employees;
    console.log(employeeList);
    return (
      <table border="1" cellSpacing="1">
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Address</th>
          <th>Birthday</th>
          <th>Gender</th>
          <th>Has Account</th>
        </tr>
        {employeeList.map((employee, index) =>
          <tr key={index}>
            <td>{employee.id}</td>
            <td>{employee.name}</td>
            <td>{employee.address}</td>
            <td>{employee.birthDay}</td>
            <td>{employee.gender}</td>
            <td>{employee.hasAccount ? "True" : "False"}</td>
          </tr>
        )}
      </table>
    );
  }
}
